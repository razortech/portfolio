<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * Localized language
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'W4yQ`YwSajCiOQ&s_6N%OqrM%q=q.{mni`]fla.5oIVe7r,|!x8ve/kO>EYN;2+4' );
define( 'SECURE_AUTH_KEY',   'pO0ugcab8b=;AA;#@9&nLX7SJKsE|A3n@EO06bMRIs*(XD/0eHU<89Xt`#X:{`yE' );
define( 'LOGGED_IN_KEY',     'DjQC$Y`^us3{*JFi2gkxlb-]P_&PyIVAc512)XMrbz}eI}xuoHU59;j&a%{R=>`/' );
define( 'NONCE_KEY',         'x];ANlN|b#uM9=7P-JkgdROa-4.N7*Ck3m!k6 FJ> 6liZ0~#=8@4&jbZ-Acp(se' );
define( 'AUTH_SALT',         'ac<]b{uQg5i;d$Ja.ovP1y;2>wYV]!kWow)Wx!m:c.HilD{E!n /AlaUN0v%LSDB' );
define( 'SECURE_AUTH_SALT',  'V{of$CqL[M^(#`>?V:YBFZz)`3{>3ZmML<?ed?|7kK1c*2n (t8([y&!@KJEYdfJ' );
define( 'LOGGED_IN_SALT',    'MU2yQamA_iYVWG:Tk!atRp6BrwfgQ|PZX-B<#/+1CXM*H2:Wydp&T+[r:A-6iz8c' );
define( 'NONCE_SALT',        'rN;X`wss!W3S3W~.g{R-#MmS-`-hMZKYlUFE0`o?m{*vx=.*3Jt+E.L?a:,,K.VQ' );
define( 'WP_CACHE_KEY_SALT', 'eWGy@/.j-:7jJ^xrcQ44xw i|jN wCM$T$u-8lBd0#AplJfNX(6sJ-QU-8JOyb;.' );


/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


/* Add any custom values between this line and the "stop editing" line. */



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
if ( ! defined( 'WP_DEBUG' ) ) {
	define( 'WP_DEBUG', false );
}

define( 'WP_ENVIRONMENT_TYPE', 'local' );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
