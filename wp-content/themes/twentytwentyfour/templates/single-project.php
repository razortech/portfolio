<?php
// Get all needed fields
$project_title   = get_the_title();
$project_content = apply_filters( 'the_content', get_the_content() );
$project_image   = get_field( 'project_image' );
$project_website = get_field( 'link_to_website' );
$project_github  = get_field( 'github_link' );

?>
<h1><?php echo esc_html( $project_title ); ?></h1>
<p><?php echo $project_content; ?></p>
<h3><?php echo esc_html( $project_website ); ?></h3>
<h3><?php echo esc_html( $project_github ); ?></h3>

<?php