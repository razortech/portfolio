const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const config = {
    // Entry point of your application
    entry: {
        main: './src/js/index.js',
        style: './src/scss/style.scss',
    },
    // Output configuration
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: (pathData) => {
            return pathData.chunk.name === 'main' ? 'bundle.js' : '[name]/[name].js';
        },
        clean: true,
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: 'babel-loader',
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, // instead of style-loader
                    'css-loader', // translates CSS into CommonJS
                    'sass-loader', // compiles Sass to CSS, using Node Sass by default
                ],
            },
        ],
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name]/[name].css', // Output CSS files
        }),
    ],

    // Mode can be 'development' or 'production'
    mode: 'development',
}



module.exports = config;
